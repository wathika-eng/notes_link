import { Button } from "flowbite-react";
import { HiArrowUp } from 'react-icons/hi';

const Content = () => {
    return (
        <div className="min-h-auto container p-2 px-10">
            <p>The notes are hosted on <b>Mediafire</b></p>
            <Button gradientMonochrome="info" className="inline-flex items-center">
                <a target="_blank" href="https://www.mediafire.com/folder/y876npv6rg887/Computer+science+materials" className="mr-2">
                    Click here to access them
                </a>
                <HiArrowUp className="text-xl" />
            </Button>
            <p className="mt-5">
                GitHub repo to Mediafire bulk downloader:
                <Button gradientMonochrome="info">
                    <a target="_blank" href="https://github.com/NicKoehler/mediafire_bulk_downloader#setup">GitHub link</a>
                </Button>
            </p>
            <p className="mt-5">
                Watch a short demo on how to download them in bulk(all at once):
            </p>
            <div className="relative w-full " style={{ paddingBottom: "56.250%" }}>
                <iframe
                    className="absolute inset-0 w-full h-full"
                    allow="fullscreen"
                    allowFullScreen
                    src="https://streamable.com/e/1r0gl1?loop=0&muted=1"
                    style={{ border: "none", width: "100%", height: "100%", overflow: "hidden" }}
                ></iframe>
            </div>

        </div>
    );
};

export default Content;
