import { Navbar, DarkThemeToggle } from "flowbite-react";

const MainNav = () => {
    return (

        <Navbar fluid>
            {/* <Navbar.Brand href="https://flowbite-react.com">
                <img src="/favicon.svg" className="mr-3 h-6 sm:h-9" alt="Flowbite React Logo" />
                <span className="self-center whitespace-nowrap text-xl font-semibold dark:text-white">Flowbite React</span>
            </Navbar.Brand> */}
            <div className="flex md:order-2">
                {/* <Button>Request</Button> */}
                <Navbar.Toggle />
            </div>
            <Navbar.Collapse>
                <Navbar.Link href="#" active>
                    Home
                </Navbar.Link>
                <Navbar.Link href="#">About</Navbar.Link>
                <Navbar.Link href="#">Services</Navbar.Link>
                <Navbar.Link href="#">Pricing</Navbar.Link>
                <Navbar.Link href="#">Contact</Navbar.Link>
                <DarkThemeToggle />
            </Navbar.Collapse>
        </Navbar>

    );
};

export default MainNav;