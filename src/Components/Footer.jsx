import { Footer } from "flowbite-react";
import { FaCircle, FaTwitterSquare, FaGithub, FaLinkedin } from "react-icons/fa";
import { Alert } from "flowbite-react";
const MainFooter = () => {
    const getYear = new Date().getFullYear();
    const alert = () => {
        <Alert color="failure" icon={FaCircle}>
            <span className="font-medium">Info alert!</span> Change a few things up and try submitting again.
        </Alert>;
    };
    return (
        <Footer container className="mt-32">
            <Footer.Copyright className="text-center mt-2" href="#" by="Wathika™" year={getYear} />
            <Footer.LinkGroup>
                <Footer.Link target="_blank" href="https://github.com/wathika-eng" className="inline-flex items-center mx-2">
                    <FaGithub className="mr-1 text-2xl" />
                </Footer.Link>
                <Footer.Link target="_blank" href="https://www.linkedin.com/in/wathikawww/" className="mx-2">
                    <FaLinkedin className="mr-1 text-2xl" />
                </Footer.Link>
                <Footer.Link onClick={alert} className="mx-2">
                    <FaTwitterSquare className="mr-1 text-2xl" />
                </Footer.Link>
                <Footer.Link href="https://wathika.tech" referrerPolicy="same-origin" target="_blank"
                    className="mx-2">Contribute</Footer.Link>
            </Footer.LinkGroup>
        </Footer>
    );
};

export default MainFooter;
