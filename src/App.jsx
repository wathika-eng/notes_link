import MainNav from "./Components/Navbar";
import MainFooter from "./Components/Footer";
import Content from "./Components/Content";
import { Flowbite } from 'flowbite-react';

export default function App() {


  return (
    <Flowbite>
      <div>
        <MainNav />
        <Content />
        <MainFooter />
      </div>
    </Flowbite>
  );
}
